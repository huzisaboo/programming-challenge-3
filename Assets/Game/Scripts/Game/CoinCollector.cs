﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CoinCollector : MonoBehaviour
{
    private AudioSource m_coinCollectAudio;
    private int m_noOfCoinsCollected = 0;

    private bool m_collected = false;
    // Start is called before the first frame update
    void Start()
    {
        m_coinCollectAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(Vector3.up * 5);

        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (m_collected)
        {
            return;
        }
         
        if(collision.gameObject.GetComponent<NinjaMovement>() != null)
        {
            m_collected = true;
            GetComponent<SpriteRenderer>().enabled = false;

            if (m_coinCollectAudio != null)
            {
                if (!m_coinCollectAudio.isPlaying)
                    m_coinCollectAudio.Play();
                m_noOfCoinsCollected++;
                Destroy(this.gameObject, m_coinCollectAudio.clip.length);
            }
        }
    }
}
