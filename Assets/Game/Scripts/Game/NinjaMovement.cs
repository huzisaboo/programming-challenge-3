﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class NinjaMovement : MonoBehaviour
{
    public GameObject m_virtualPadMiddle;
    public float m_movementSpeed = 5.0f;
    public float m_movementThreshold = 0.5f;
    public Vector3 m_ninjaSpawnPosition;


    private Vector3 m_InputDirection;
    private Rigidbody2D m_rigidBody;
    private Animator m_animator;
    private bool m_tappedJoystick = false;
    private Image m_virtualPadImage;

    // Start is called before the first frame update
    void Start()
    {
        m_ninjaSpawnPosition = this.transform.position;
        m_rigidBody = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        m_virtualPadImage = m_virtualPadMiddle.transform.parent.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 0)
        {
            Touch a_touch = Input.GetTouch(0);

            if (a_touch.phase == TouchPhase.Began)
            {
              Vector2 m_touchLocation = Input.GetTouch(0).position - new Vector2(m_virtualPadMiddle.transform.parent.transform.position.x, m_virtualPadMiddle.transform.parent.transform.position.y);

                if ((m_touchLocation.x >= 0.0f && m_touchLocation.x <= m_virtualPadImage.rectTransform.rect.width) && (m_touchLocation.y >= 0.0f && m_touchLocation.y <= m_virtualPadImage.rectTransform.rect.height))
                {
                    m_tappedJoystick = true;
                }
            }
            if (a_touch.phase == TouchPhase.Moved || a_touch.phase == TouchPhase.Stationary)
            {
                if (m_tappedJoystick)
                {
                    Vector2 a_moveDirection = (Input.GetTouch(0).position - new Vector2(m_virtualPadMiddle.transform.position.x, m_virtualPadMiddle.transform.position.y)).normalized;

                    // Debug.Log("Move Dir:" + a_moveDirection);

                    m_InputDirection.x = a_moveDirection.x;
                    m_InputDirection.y = a_moveDirection.y;


                    // Debug.Log(m_InputDirection);
                }
            }
            else if (a_touch.phase == TouchPhase.Canceled || a_touch.phase == TouchPhase.Ended)
            {

                m_InputDirection = Vector2.zero;
                m_tappedJoystick = false;
            }


            if (m_InputDirection.magnitude > m_movementThreshold)
            {
                m_animator.SetFloat("InputX", m_InputDirection.x);
                m_animator.SetFloat("InputY", m_InputDirection.y);
                m_animator.SetBool("Walking", true);

            }
            else
            {
                m_animator.SetBool("Walking", false);
            }
            m_rigidBody.velocity = new Vector2(m_InputDirection.x * m_movementSpeed, m_InputDirection.y * m_movementSpeed);
        }
    }
}
