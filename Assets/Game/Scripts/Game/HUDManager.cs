﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    public Text m_coinCollection;
    public Text m_timeElapsed;
    public float m_timer = 0.0f;

    private int m_currentCoinCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        m_coinCollection.text = "Coins: " + m_currentCoinCount;
    }

    // Update is called once per frame
    void Update()
    {
       m_timer += Time.deltaTime;

       m_timeElapsed.text ="Time Elapsed: " +   System.Convert.ToInt32(m_timer).ToString()+" s";
    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<CoinCollector>() != null)
        {
            m_currentCoinCount++;
            m_coinCollection.text = "Coins: " + m_currentCoinCount;
        }
    }
}
