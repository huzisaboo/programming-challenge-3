﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private SpriteRenderer m_BulletSpriteRenderer;
    public float m_bulletSpeed = 2.0f;
    // Start is called before the first frame update
    void Start()
    {
        m_BulletSpriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(m_BulletSpriteRenderer.flipX)
        {
            if(this.transform.rotation == Quaternion.Euler(0,0,90))
            {
                this.transform.position += Vector3.up * Time.deltaTime * m_bulletSpeed;
            }
            else
            {
                this.transform.position += Vector3.right * Time.deltaTime * m_bulletSpeed;
            }

        }
        else
        {
            if (this.transform.rotation == Quaternion.Euler(0, 0, 90))
            {
                this.transform.position += Vector3.down * Time.deltaTime * m_bulletSpeed;
            }

            else
            {
                this.transform.position += Vector3.left * Time.deltaTime * m_bulletSpeed;
            }
        }
        
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(this.gameObject);
    }
}
