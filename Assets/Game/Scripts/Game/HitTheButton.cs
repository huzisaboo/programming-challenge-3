﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class HitTheButton : MonoBehaviour
{
    public List<GameObject> m_listOfFlames;
    public Sprite m_ButtonHitSprite;
    private Sprite m_ButtonOriginalSprite;
    private Tilemap m_tileMap;
    private Grid m_grid;
    private bool m_buttonHit = false;
    private AudioSource m_audio;
    private float m_time = 0.0f; 
    // Start is called before the first frame update
    void Start()
    {
        m_tileMap = this.transform.parent.GetComponent<Tilemap>();
        m_grid = this.transform.GetComponentInParent<Grid>();

        m_audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(m_audio.isPlaying)
        {
            m_time += Time.deltaTime;
            if(m_time >= m_audio.clip.length /4)    //Since the audio length is quite long, I had to reduce its play time by a quarter
            {
                m_audio.Stop();

                if (m_listOfFlames.Count > 0)
                {
                    foreach (GameObject a_flame in m_listOfFlames)
                    {
                        a_flame.SetActive(true);
                    }
                }

                m_time = 0;

                
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<NinjaMovement>() != null)
        {
            if (m_buttonHit)
            {
                return;
            }

            Vector3Int a_tilePosition = m_grid.WorldToCell(this.transform.position + Vector3.left);
            Tile a_tile = (Tile)m_tileMap.GetTile(a_tilePosition);
            m_ButtonOriginalSprite = a_tile.sprite;

            a_tile.sprite = m_ButtonHitSprite;

            m_tileMap.RefreshAllTiles();

            m_buttonHit = true;


            m_audio.Play();

        }
    }

    private void OnApplicationQuit()
    {
        if(m_ButtonOriginalSprite!=null)
        {
            Vector3Int a_tilePosition = m_grid.WorldToCell(this.transform.position + Vector3.left);
            Tile a_tile = (Tile)m_tileMap.GetTile(a_tilePosition);

            a_tile.sprite = m_ButtonOriginalSprite;
            m_tileMap.RefreshAllTiles();
        }
    }

}