﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    private NinjaMovement m_player;
    private AudioSource m_audio;
    // Start is called before the first frame update
    void Start()
    {
        m_audio = GetComponent<AudioSource>();

        if(m_audio.clip != null)
        {
            m_audio.Play();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_player = collision.gameObject.GetComponent<NinjaMovement>();
        if(m_player != null)
        {
            collision.gameObject.transform.position = m_player.m_ninjaSpawnPosition;
        }
    }
}
