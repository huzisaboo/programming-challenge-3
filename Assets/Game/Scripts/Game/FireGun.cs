﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGun : MonoBehaviour
{
    public GameObject m_bullet;
    public float m_timeThreshold = 0.5f;
    public float m_minDistanceThreshold = 200.0f;
    public float m_maxDistanceThreshold = 600.0f;

    private SpriteRenderer m_FireSpriteRenderer;
    private NinjaMovement m_ninjaMovementReference;
    private Animator m_animator;
    private bool m_rotateFire = false;
    private Vector2 m_initialTouchPosition;
    private Vector2 m_finalTouchPosition;
    private float m_timer = 0.0f;
    private float m_touchStartTime = 0.0f;
    private float m_touchEndTime = 0.0f;
    
    private bool m_swiped = false;
    // Start is called before the first frame update
    void Start()
    {
        m_FireSpriteRenderer = m_bullet.GetComponent<SpriteRenderer>();
        m_animator = GetComponent<Animator>();
        m_ninjaMovementReference = GetComponent<NinjaMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        m_timer += Time.deltaTime;
        if(Input.touchCount > 0)
        {
            foreach(Touch a_touch in Input.touches)
            {

                if (a_touch.phase == TouchPhase.Began)
                {
                    m_initialTouchPosition = Input.mousePosition;
                    m_touchStartTime = m_timer;

                }

                else if (a_touch.phase == TouchPhase.Ended) //Check for valid swipe
                {
                    m_finalTouchPosition = Input.mousePosition;

                    float a_distance = Vector2.Distance(m_finalTouchPosition, m_initialTouchPosition);
                    m_touchEndTime = m_timer;
                    float a_timedifference = m_touchEndTime - m_touchStartTime;
                    //Debug.Log(a_timedifference);
                    //Debug.Log(a_distance);
                    if (a_timedifference <= m_timeThreshold && (a_distance > m_minDistanceThreshold && a_distance <= m_maxDistanceThreshold))
                    {
                        m_swiped = true;
                    }

                }
            }
            
        }

        if (m_swiped)
        {
            if (m_animator != null)
            {
                m_animator.SetTrigger("Attack");
                if(m_animator.GetFloat("InputX") >= m_ninjaMovementReference.m_movementThreshold)
                {
                    if(m_rotateFire)
                    {
                        m_bullet.transform.Rotate(Vector3.forward, -90);
                        m_rotateFire = false;
                    }

                    m_FireSpriteRenderer.flipX = true;
                    Instantiate(m_bullet, this.transform.position + new Vector3(1,0,0), this.transform.rotation);
                }
                else if(m_animator.GetFloat("InputX") <= (-m_ninjaMovementReference.m_movementThreshold))
                {
                    if (m_rotateFire)
                    {
                        m_bullet.transform.Rotate(Vector3.forward, -90);
                        m_rotateFire = false;
                    }

                    m_FireSpriteRenderer.flipX = false;
                    Instantiate(m_bullet, this.transform.position + new Vector3(-1, 0, 0), this.transform.rotation);
                }
                else if (m_animator.GetFloat("InputY") >= (m_ninjaMovementReference.m_movementThreshold))
                {
                    m_FireSpriteRenderer.flipX = true;
                    Instantiate(m_bullet, this.transform.position + new Vector3(0, 1, 0), Quaternion.Euler(0,0,90));
                    
                }
                else if (m_animator.GetFloat("InputY") <= (-m_ninjaMovementReference.m_movementThreshold))
                {
                    m_FireSpriteRenderer.flipX = false;
                    Instantiate(m_bullet, this.transform.position + new Vector3(0, -1, 0), Quaternion.Euler(0, 0, 90));
      
                   
                }
             
                else
                {
                    m_FireSpriteRenderer.flipX = false;
                    Instantiate(m_bullet, this.transform.position + new Vector3(0, -1, 0), Quaternion.Euler(0, 0, 90));
                }
            }

            m_swiped = false;
        }
    }

    
}
